using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.UserProfile as Upf;
using Toybox.Application as App;
using Toybox.AntPlus as Apl;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class zpotView extends Ui.DataField
{
    //"escuchante" de los datos extra del ámbito de la potencia
    var listener=null;
    //objeto de la clase BikePower
    var bikePower=null;
    //número de valores de potencia de trabajo, número de zonas de potencia,
    //FTP, potencia máxima e identificador de dato a imprimir en pantalla
    //los valores por defecto de estas variables y otros parámetros se definen
    //en el fichero resources/properties.xml
    var np;
    var izp;
    var ftp;
    var pm;
    var gpa;
    //vectores de zonas de potencia y de potencia y datos relacionados de los
    //que se calculará la media
    var zp=null;
    var pot=null;
    var gpal=null;
    var gpar=null;
    var etl=null;
    var etr=null;
    //variables necesarias para el cálculo de la GPA
    var datosBal=null;
    var datosPar=null;
    var bal=0.0;
    var etlins=0.0;
    var etrins=0.0;
    var gprl=0.0;
    var gprr=0.0;
    var gpalins=0.0;
    var gparins=0.0;
    //potencia instantánea (media de la pedalada completa)
    var pins=0.0;
    //potencia media de la actividad completa
    var pmed=0.0;
    //peso del atleta
    var peso=0.0;
    //zonas de potencia
    var zins=0.0;
    var zmed=0.0;
    //W/kg
    var wkgi=0.0;
    var wkgm=0.0;
    //variables auxiliares
    var i=0;
    var aux=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función de inicialización del campo de datos
    function initialize()
    {
        //inicializo el campo
        DataField.initialize();
        //extraigo los valores de configuración
        np = App.Properties.getValue("np_val");
        izp = App.Properties.getValue("izp_val");
        ftp = App.Properties.getValue("ftp_val");
        pm = App.Properties.getValue("pm_val");
        gpa = App.Properties.getValue("gpa_val");
        //compruebo si los datos Number se han leído correctamente; punto 10 de
        //https://forums.garmin.com/forum/developers/connect-iq/120212-new-developer-faq
        if(!(np instanceof Number))
        {
            np = np.toNumber();
        }
        if(!(izp instanceof Number))
        {
            izp = izp.toNumber();
        }
        if(!(ftp instanceof Number))
        {
            ftp = ftp.toNumber();
        }
        if(!(pm instanceof Number))
        {
            pm = pm.toNumber();
        }
        if(!(gpa instanceof Number))
        {
            gpa = gpa.toNumber();
        }
        //extraigo el peso del atleta (viene en gramos)
        peso = Upf.getProfile().weight/1000.0;
        //comprobamos errores en la lectura
        if((peso==null)||(peso<=0.0))
        {
            //asignamos el valor 1 a la variable para no dividir entre 0 nunca
            peso = 1.0;
        }
        //si se pasan 8 zonas digo que son 9
        if(izp==8)
        {
            izp = 9;
        }
        //compruebo si hay que mostrar GPA o efectividad
        if(gpa!=0)
        {
            //inicializo el "escuchante" de los datos de potencia extra
            listener = new Apl.BikePowerListener();
            bikePower = new Apl.BikePower(listener);
            //compruebo el tipo de dato a mostrar
            if(gpa==1)
            {
                //dimensiono los vecrores de GPA y los inicializo
                gpal = new[np];
                gpar = new[np];
                for(i=0;i<np;i++)
                {
                    gpal[i] = 0.0;
                    gpar[i] = 0.0;
                }
            }
            else
            {
                //dimensiono los vecrores de efectividad y los inicializo
                etl = new[np];
                etr = new[np];
                for(i=0;i<np;i++)
                {
                    etl[i] = 0.0;
                    etr[i] = 0.0;
                }
            }
        }
        else
        {
            //dimensiono el vector de zonas de potencia y lo inicializo
            zp = new[izp];
            if(izp==7)
            {
                zp[0] = ftp*0.55;
                zp[1] = ftp*0.75;
                zp[2] = ftp*0.90;
                zp[3] = ftp*1.05;
                zp[4] = ftp*1.20;
                zp[5] = ftp*1.50;
                zp[6] = pm*1.0;
            }
            else
            {
                zp[0] = ftp*0.55;
                zp[1] = ftp*0.75;
                zp[2] = ftp*0.90;
                zp[3] = ftp*1.05;
                zp[4] = ftp*1.20;
                zp[5] = ftp*1.30;
                zp[6] = ftp*1.40;
                zp[7] = ftp*1.50;
                zp[8] = pm*1.0;
            }
        }
        //dimensiono el vector de potencia y lo inicializo
        pot = new[np];
        for(i=0;i<np;i++)
        {
            pot[i] = 0.0;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función para calcular la zona de potencia donde estamos
    function calcZona(potencia)
    {
        //valor a calcular
        var valor=0.0;
        //voy comprobando las zonas (7 niveles los hay seguro)
        if(potencia<=zp[0])
        {
            //zona 1
            aux = zp[0]*1.0;
            valor = (aux!=0.0) ? 1.0+potencia/aux : 0.0;
        }
        else if((potencia>zp[0])&&(potencia<=zp[1]))
        {
            //zona 2
            aux = (zp[1]-zp[0])*1.0;
            valor = (aux!=0.0) ? 2.0+(potencia-zp[0])/aux : 0.0;
        }
        else if((potencia>zp[1])&&(potencia<=zp[2]))
        {
            //zona 3
            aux = (zp[2]-zp[1])*1.0;
            valor = (aux!=0.0) ? 3.0+(potencia-zp[1])/aux : 0.0;
        }
        else if((potencia>zp[2])&&(potencia<=zp[3]))
        {
            //zona 4
            aux = (zp[3]-zp[2])*1.0;
            valor = (aux!=0.0) ? 4.0+(potencia-zp[2])/aux : 0.0;
        }
        else if((potencia>zp[3])&&(potencia<=zp[4]))
        {
            //zona 5
            aux = (zp[4]-zp[3])*1.0;
            valor = (aux!=0.0) ? 5.0+(potencia-zp[3])/aux : 0.0;
        }
        else if((potencia>zp[4])&&(potencia<=zp[5]))
        {
            //zona 6
            aux = (zp[5]-zp[4])*1.0;
            valor = (aux!=0.0) ? 6.0+(potencia-zp[4])/aux : 0.0;
        }
        else if((potencia>zp[5])&&(potencia<=zp[6]))
        {
            //zona 7
            aux = (zp[6]-zp[5])*1.0;
            valor = (aux!=0.0) ? 7.0+(potencia-zp[5])/aux : 0.0;
        }
        //compruebo si trabajo con 9 niveles
        if(izp==9)
        {
            //compruebo los dos niveles que quedan
            if((potencia>zp[6])&&(potencia<=zp[7]))
            {
                //zona 8
                aux = (zp[7]-zp[6])*1.0;
                valor = (aux!=0.0) ? 8.0+(potencia-zp[6])/aux : 0.0;
            }
            else if((potencia>zp[7])&&(potencia<=zp[8]))
            {
                //zona 8
                aux = (zp[8]-zp[7])*1.0;
                valor = (aux!=0.0) ? 9.0+(potencia-zp[7])/aux : 0.0;
            }
        }
        //devolvemos el valor
        return valor;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función para actualizar un vector y calcular la media
    function calcMedia(vector,dato,nDatos)
    {
        //valor de salida
        var media=0.0;
        //hago hueco en el vector para el nuevo dato
        for(i=1;i<nDatos;i++)
        {
            vector[i-1] = vector[i];
        }
        //asigno el nuevo dato
        vector[nDatos-1] = dato;
        //media de los valores del vector
        for(i=0;i<nDatos;i++)
        {
            media += vector[i]*1.0;
        }
        media /= (nDatos*1.0);
        //devolvemos el valor
        return media;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // The given info object contains all the current workout information.
    // Calculate a value and save it locally in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    function compute(info)
    {
        // See Activity.Info in the documentation for available information.
        //compruebo si tenemos potencia
        if(info has :currentPower)
        {
            //extraigo la potencia instantánea
            pins = info.currentPower;
            //compruebo si el dato se ha leído
            if(pins!=null)
            {
                //actualizo el vector y calculo la media
                pins = calcMedia(pot,pins,np);
                //calculo el valor instantáneo de potencia relativa
                wkgi = pins/peso;
                //compruebo si hay potencia media
                pmed = 0.0;
                if(info has :averagePower)
                {
                    //extraigo la potencia media
                    pmed = info.averagePower;
                    if(pmed==null)
                    {
                        pmed = 0.0;
                    }
                }
                //calculo el valor medio de potencia relativa
                wkgm = pmed/peso;
                ////////////////////////////////////////////////////////////////
                //compruebo el tipo de dato a extraer
                if(gpa==0)
                {
                    //zona de potencia instantánea
                    zins = calcZona(pins);
                    //compruebo si hay dato de potencia media
                    if(pmed!=0.0)
                    {
                        //zona de la potencia media
                        zmed = calcZona(pmed);
                    }
                }
                else
                {
                    //extraemos balance y efectividad
                    datosBal =bikePower.getPedalPowerBalance();
                    datosPar =bikePower.getTorqueEffectivenessPedalSmoothness();
                    //efectividad hay que extraerla siempre
                    if(datosPar!=null)
                    {
                        etlins = datosPar.leftTorqueEffectiveness;
                        etrins = datosPar.rightTorqueEffectiveness;
                    }
                    else
                    {
                        gpalins = 0.0;
                        gparins = 0.0;
                        etlins = 0.0;
                        etrins = 0.0;
                    }
                    //compruebo si hay que calcular GPA
                    if(gpa==1)
                    {
                        //extraemos los datos de balance, si ha lugar
                        //pongo los valores de efectividad mayores o iguales a 1
                        //para no dividir entre números menores que 1
                        if((datosBal!=null)&&(etlins>=1.0)&&(etrins>=1.0))
                        {
                            //extraemos el dato de balance
                            if(datosBal.rightPedalIndicator)
                            {
                                bal = 100.0-datosBal.pedalPowerPercent;
                            }
                            else
                            {
                                bal = datosBal.pedalPowerPercent;
                            }
                            if(bal!=null)
                            {
                                //calculo los GPR
                                gprl = (pins*bal)/etlins;
                                gprr = (pins*(100.0-bal))/etrins;
                                //calculo el dato instantáneo de GPA
                                gpalins = gprl-pins*bal/100.0;
                                gparins = gprr-pins*(1.0-bal/100.0);
                                //no permito que GPA sea mayor que la potencia
                                if((gpalins>(pins*bal/100.0))||
                                   (gparins>(pins*(1.0-bal/100.0))))
                                {
                                    gpalins = pins*bal/100.0;
                                    gparins = pins*(1.0-bal/100.0);
                                }
                                //actualizo los valores y calculo las medias
                                gpalins = calcMedia(gpal,gpalins,np);
                                gparins = calcMedia(gpar,gparins,np);
                            }
                            else
                            {
                                gpalins = 0.0;
                                gparins = 0.0;
                            }
                        }
                        else
                        {
                            gpalins = 0.0;
                            gparins = 0.0;
                        }
                        //multiplico la GPA por -1 para que salga negativa
                        gpalins *= -1.0;
                        gparins *= -1.0;
                    }
                    else
                    {
                        //actualizo los valores de par y calculo las medias
                        etlins = calcMedia(etl,etlins,np);
                        etrins = calcMedia(etr,etrins,np);
                    }
                }
            }
            else
            {
                //si no se ha leído dato de potencia
                pins = 0.0;
                wkgi = 0.0;
                zins = 0.0;
                gpalins = 0.0;
                gparins = 0.0;
                etlins = 0.0;
                etrins = 0.0;
            }
        }
        else
        {
            //si no hay dato de potencia
            pins = 0.0;
            wkgi = 0.0;
            zins = 0.0;
            gpalins = 0.0;
            gparins = 0.0;
            etlins = 0.0;
            etrins = 0.0;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
    function onUpdate(dc)
    {
        // Set the background color
        View.findDrawableById("Background").setColor(getBackgroundColor());
        //variables para escribir los valores de trabajo
        var labelPot=View.findDrawableById("labelPot");
        var valuePot=View.findDrawableById("valuePot");
        var labelPotW=View.findDrawableById("labelPotW");
        var valuePotN=View.findDrawableById("valuePotN");
        var labelPotS=View.findDrawableById("labelPotS");
        var valueZonaI=View.findDrawableById("valueZonaI");
        var valueZonaM=View.findDrawableById("valueZonaM");
        var valueGPAL=View.findDrawableById("valueGPAL");
        var valueGPAR=View.findDrawableById("valueGPAR");
        var valueEizq=View.findDrawableById("valueEizq");
        var valueEdch=View.findDrawableById("valueEdch");
        var labelEizq=View.findDrawableById("labelEizq");
        var labelEdch=View.findDrawableById("labelEdch");
        var valueWkgI=View.findDrawableById("valueWkgI");
        var valueWkgM=View.findDrawableById("valueWkgM");
        //color del texto con respecto al fondo
        if(getBackgroundColor()==Gfx.COLOR_BLACK)
        {
            //blanco sobre fondo negro
            labelPot.setColor(Gfx.COLOR_WHITE);
            valuePot.setColor(Gfx.COLOR_WHITE);
            labelPotW.setColor(Gfx.COLOR_WHITE);
            valuePotN.setColor(Gfx.COLOR_WHITE);
            labelPotS.setColor(Gfx.COLOR_WHITE);
            if(gpa==0)
            {
                valueZonaI.setColor(Gfx.COLOR_WHITE);
                valueZonaM.setColor(Gfx.COLOR_WHITE);
            }
            else if(gpa==1)
            {
                valueGPAL.setColor(Gfx.COLOR_WHITE);
                valueGPAR.setColor(Gfx.COLOR_WHITE);
            }
            else
            {
                valueEizq.setColor(Gfx.COLOR_WHITE);
                valueEdch.setColor(Gfx.COLOR_WHITE);
                labelEizq.setColor(Gfx.COLOR_WHITE);
                labelEdch.setColor(Gfx.COLOR_WHITE);
            }
            valueWkgI.setColor(Gfx.COLOR_WHITE);
            valueWkgM.setColor(Gfx.COLOR_WHITE);
        }
        else
        {
            //negro sobre fondo blanco
            labelPot.setColor(Gfx.COLOR_BLACK);
            valuePot.setColor(Gfx.COLOR_BLACK);
            labelPotW.setColor(Gfx.COLOR_BLACK);
            valuePotN.setColor(Gfx.COLOR_BLACK);
            labelPotS.setColor(Gfx.COLOR_BLACK);
            if(gpa==0)
            {
                valueZonaI.setColor(Gfx.COLOR_BLACK);
                valueZonaM.setColor(Gfx.COLOR_BLACK);
            }
            else if(gpa==1)
            {
                valueGPAL.setColor(Gfx.COLOR_BLACK);
                valueGPAR.setColor(Gfx.COLOR_BLACK);
            }
            else
            {
                valueEizq.setColor(Gfx.COLOR_BLACK);
                valueEdch.setColor(Gfx.COLOR_BLACK);
                labelEizq.setColor(Gfx.COLOR_BLACK);
                labelEdch.setColor(Gfx.COLOR_BLACK);
            }
            valueWkgI.setColor(Gfx.COLOR_BLACK);
            valueWkgM.setColor(Gfx.COLOR_BLACK);
        }
        //escribo los datos
        valuePot.setText(pins.format("%d"));
        valuePotN.setText(np.format("%d"));
        if(gpa==0)
        {
            valueZonaI.setText(zins.format("%.1f"));
            valueZonaM.setText(zmed.format("%.1f"));
        }
        else if(gpa==1)
        {
            valueGPAL.setText(gpalins.format("%+d"));
            valueGPAR.setText(gparins.format("%+d"));
        }
        else
        {
            valueEizq.setText(etlins.format("%d"));
            valueEdch.setText(etrins.format("%d"));
            labelEizq.setText(Rez.Strings.labelEizq);
            labelEdch.setText(Rez.Strings.labelEdch);
        }
        valueWkgI.setText(wkgi.format("%.2f"));
        valueWkgM.setText(wkgm.format("%.2f"));
        //escribo las etiquetas
        labelPot.setText(Rez.Strings.labelPot);
        labelPotW.setText(Rez.Strings.labelPotW);
        labelPotS.setText(Rez.Strings.labelPotS);
        // Call parent's onUpdate(dc) to redraw the layout
        View.onUpdate(dc);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Set your layout here. Anytime the size of obscurity of
    // the draw context is changed this will be called.
    function onLayout(dc)
    {
        //esto no sé lo que hace
        View.setLayout(Rez.Layouts.MainLayout(dc));
        //variables para configurar la visualización
        View.findDrawableById("labelPot");
        View.findDrawableById("valuePot");
        View.findDrawableById("labelPotW");
        View.findDrawableById("valuePotN");
        View.findDrawableById("labelPotS");
        if(gpa==0)
        {
            View.findDrawableById("valueZonaI");
            View.findDrawableById("valueZonaM");
        }
        else if(gpa==1)
        {
            View.findDrawableById("valueGPAL");
            View.findDrawableById("valueGPAR");
        }
        else
        {
            View.findDrawableById("labelEizq");
            View.findDrawableById("labelEdch");
            View.findDrawableById("valueEizq");
            View.findDrawableById("valueEdch");
        }
        View.findDrawableById("valueWkgI");
        View.findDrawableById("valueWkgM");
        ////////////////////////////////////////////////////////////////////////
        return true;
    }
}
