using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class presionView extends Ui.DataField
{
    //presión filtrada y sin filtrar
    var pf=0.0;
    var psf=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función de inicialización del campo de datos
    function initialize()
    {
        //inicializo el campo
        DataField.initialize();
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // The given info object contains all the current workout information.
    // Calculate a value and save it locally in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    function compute(info)
    {
        // See Activity.Info in the documentation for available information.
        //compruebo si tenemos los datos de trabajo
        if((info has :ambientPressure)&&(info has :rawAmbientPressure))
        {
            //extraigo los datos
            pf = info.ambientPressure;
            psf = info.rawAmbientPressure;
            //compruebo si los datos se han leído
            if((pf!=null)&&(psf!=null))
            {
                //convierto a hPa
                pf /= 100.0;
                psf /= 100.0;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
    function onUpdate(dc)
    {
        // Set the background color
        View.findDrawableById("Background").setColor(getBackgroundColor());
        //variables para escribir los valores de trabajo
        var pfi=View.findDrawableById("pf");
        var psfi=View.findDrawableById("psf");
        var labelPf=View.findDrawableById("labelPf");
        var labelPsf=View.findDrawableById("labelPsf");
        //color del texto con respecto al fondo
        if(getBackgroundColor()==Gfx.COLOR_BLACK)
        {
            //blanco sobre fondo negro
            pfi.setColor(Gfx.COLOR_WHITE);
            psfi.setColor(Gfx.COLOR_WHITE);
            labelPf.setColor(Gfx.COLOR_WHITE);
            labelPsf.setColor(Gfx.COLOR_WHITE);
        }
        else
        {
            //negro sobre fondo blanco
            pfi.setColor(Gfx.COLOR_BLACK);
            psfi.setColor(Gfx.COLOR_BLACK);
            labelPf.setColor(Gfx.COLOR_BLACK);
            labelPsf.setColor(Gfx.COLOR_BLACK);
        }
        //escribo los datos
        pfi.setText(pf.format("%.2f"));
        psfi.setText(psf.format("%.2f"));
        // Call parent's onUpdate(dc) to redraw the layout
        View.onUpdate(dc);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Set your layout here. Anytime the size of obscurity of
    // the draw context is changed this will be called.
    function onLayout(dc)
    {
        //esto no sé lo que hace
        View.setLayout(Rez.Layouts.MainLayout(dc));
        //variables para configurar la visualización
        var pfi=View.findDrawableById("pf");
        var psfi=View.findDrawableById("psf");
        var labelPf=View.findDrawableById("labelPf");
        var labelPsf=View.findDrawableById("labelPsf");
        //coloco los elemento uno encima del otro
        pfi.locY = pfi.locY-9;
        pfi.locX = pfi.locX+34;
        psfi.locY = psfi.locY+17;
        psfi.locX = psfi.locX+34;
        labelPf.locY = labelPf.locY-9;
        labelPf.locX = labelPf.locX+60;
        labelPsf.locY = labelPsf.locY+15;
        labelPsf.locX = labelPsf.locX+65;
        //visualizamos las etiquetas
        View.findDrawableById("labelPf").setText(Rez.Strings.labelPf);
        View.findDrawableById("labelPsf").setText(Rez.Strings.labelPsf);
        ////////////////////////////////////////////////////////////////////////
        return true;
    }
}
