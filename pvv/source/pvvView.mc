using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Math as Mat;
using Toybox.Application as App;
using Toybox.System as Sys;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class pvvView extends Ui.DataField
{
    //identificador de intento de buscar errores groseros
    //variable de trabajo (0/1/2->rawAmbientPressure, ambientPressure, altitude)
    //número máximo de datos de trabajo (ha de ser SIEMPRE>=3) y longitudes
    //máxima y mínima del segmento de cálculo
    //los valores por defecto de estas variables y otros parámetros se definen
    //en el fichero resources/properties.xml
    var buscaEGros;
    var vt;
    var datosMax;
    var dmax;
    var dmin;
    //vectores para almacenar datos, pesos y residuos
    var vectorHP=null;
    var vectorD=null;
    var vectorT=null;
    var pesos=null;
    var residuos=null;
    //primera y última posición con dato de los vectores
    var primero=0;
    var ultimo=0;
    //elementos de la matriz normal para el ajuste MMCC de la recta
    var N11=0.0;
    var N12=0.0;
    var N22=0.0;
    //elementos del vector de términos independientes del sistema normal
    var d1=0.0;
    var d2=0.0;
    //parámetros de la recta y=a*x+b
    var a=0.0;
    var b=0.0;
    //altitud, distancia, tiempo y variables auxiliares
    var hp=0.0;
    var dh=0.0;
    var d=0.0;
    var t=0.0;
    var p1=0.0;
    var p2=0.0;
    //datos a visualizar
    var pend=0.0;
    var vver=0.0;
    //unidades del sistema internacional
    var usaSI=1;
    //índices de trabajo
    var i=0;
    var j=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función de inicialización del campo de datos
    function initialize()
    {
        //inicializo el campo
        DataField.initialize();
        //extraigo los valores de configuración
        vt = App.Properties.getValue("vt_val");
        datosMax = App.Properties.getValue("datosMax_val");
        buscaEGros = App.Properties.getValue("buscaEGros_val");
        dmax = App.Properties.getValue("dmax_val");
        dmin = App.Properties.getValue("dmin_val");
        //compruebo si los datos Number se han leído correctamente; punto 10 de
        //https://forums.garmin.com/forum/developers/connect-iq/120212-new-developer-faq
        if(!(vt instanceof Number))
        {
            vt = vt.toNumber();
        }
        if(!(datosMax instanceof Number))
        {
            datosMax = datosMax.toNumber();
        }
        //compruebo si la distancia máxima es menor que la mínima
        if(dmax<dmin)
        {
            //asigno el doble que la distancia mínima
            dmax = 2.0*dmin;
        }
        //inicializo los vectores a datosMax elementos
        vectorHP=new[datosMax];
        vectorD=new[datosMax];
        vectorT=new[datosMax];
        pesos=new[datosMax];
        residuos=new[datosMax];
        //extraigo la configuración del dispositivo
        if(Sys.getDeviceSettings().distanceUnits==Sys.UNIT_STATUTE)
        {
            usaSI = 0;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función para el ajuste MMCC
    function ajustaMMCC()
    {
        //coeficientes del sistema normal
        N11 = 0.0;
        N12 = 0.0;
        N22 = pesos[primero];
        d1 = 0.0;
        d2 = 0.0;
        //empiezo el sumatorio en primero+1 porque he reducido las
        //distancias y presiones o altitudes al punto inicial, luego los valores
        //correspondientes a j=primero son 0
        //el valor correspondiente a N22 para el primer punto ya
        //está tenido en cuenta en la inicialización de N22
        for(j=primero+1;j<=ultimo;j++)
        {
            N11 += (vectorD[j]-vectorD[primero])*
                   (vectorD[j]-vectorD[primero])*pesos[j];
            N12 += (vectorD[j]-vectorD[primero])*pesos[j];
            N22 += pesos[j];
            d1 += (vectorD[j]-vectorD[primero])*
                  (vectorHP[j]-vectorHP[primero])*pesos[j];
            d2 += (vectorHP[j]-vectorHP[primero])*pesos[j];
        }
        //resuelvo el sistema
        b = (N12*d1-N11*d2)/(N12*N12-N11*N22);
        //pendiente de la recta
        a = (d2-N22*b)/N12;
        //salimos de la función
        return;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay que volver a hacer el ajuste porque algún residuo sea
    //demasiado grande
    //la función devuelve el vector de pesos actualizado
    function vuelveAAjustar()
    {
        //variable identificadora para recalcular
        var recalcula=false;
        //norma al cuadrado del vector de residuos
        var vtv=0.0;
        //desviación típica del observable de peso unidad a posteriori
        var s0p=0.0;
        ////////////////////////////////////////////////////////////////////////
        //calculamos el vector de residuos del ajuste
        for(j=primero;j<=ultimo;j++)
        {
            //calculo el residuo
            residuos[j] = vectorHP[j]-vectorHP[primero]-
                          (a*(vectorD[j]-vectorD[primero])+b);
            //lo paso a valor absoluto
            residuos[j] *= (residuos[j]<0.0)?-1.0:1.0;
            //voy calculando vtv
            vtv += residuos[j]*residuos[j];
        }
        //desv. típica de referencia del observable de peso unidad a posteriori
        s0p = Mat.sqrt(vtv/(ultimo-primero+1.0-2.0));
        //reasignación de pesos por el método danés
        for(j=primero;j<=ultimo;j++)
        {
            //compruebo si hay que reasignar peso
            if(residuos[j]>(2.0*s0p))
            {
                //nuevo peso
                pesos[j] = Mat.pow(E,-residuos[j]*residuos[j]/(2.0*s0p*s0p));
                //indicamos que hay que recalcular
                recalcula = true;
            }
        }
        //devolvemos el identificador de racálculo
        return recalcula;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // The given info object contains all the current workout information.
    // Calculate a value and save it locally in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    function compute(info)
    {
        // See Activity.Info in the documentation for available information.
        //compruebo si tenemos altitud y distancia recorrida
        if((((vt==0)&&(info has :rawAmbientPressure))||
            ((vt==1)&&(info has :ambientPressure))||
            ((vt==2)&&(info has :altitude)))&&
           (info has :elapsedDistance)&&(info has :elapsedTime))
        {
            //extraigo la presión o la altitud
            if(vt==0)
            {
                //extraigo la presión sin filtrar
                hp = info.rawAmbientPressure;
            }
            else if(vt==1)
            {
                //extraigo la presión filtrada
                hp = info.ambientPressure;
            }
            else if(vt==2)
            {
                //extraigo la altitud
                hp = info.altitude;
            }
            //extraigo la distancia y el tiempo
            d = info.elapsedDistance;
            t = info.elapsedTime*1.0;
            //compruebo si los datos se han leído
            if((hp!=null)&&(d!=null)&&(t!=null))
            {
                //paso el tiempo a segundos
                t /= 1000.0;
                //compruebo el número de datos de los vectores
                if(i>=datosMax)
                {
                    //ya están llenos: espacio para los nuevos elementos
                    for(j=1;j<datosMax;j++)
                    {
                        //muevo todos los elementos hacia atrás
                        vectorHP[j-1] = vectorHP[j];
                        vectorD[j-1] = vectorD[j];
                        vectorT[j-1] = vectorT[j];
                    }
                    //posición del último elemento de los vectores de datos
                    ultimo = datosMax-1;
                }
                else
                {
                    //posición del último elemento de los vectores de datos
                    ultimo = i;
                    //aumento el contador de datos
                    i++;
                }
                //introduzco los datos en los vectores
                vectorHP[ultimo] = hp;
                vectorD[ultimo] = d;
                vectorT[ultimo] = t;
                ////////////////////////////////////////////////////////////////
                //sólo continúo si el vector se ha llenado y si entre el primero
                //y el último puntos se supera la distancia mínima
                if((ultimo==(datosMax-1))&&((vectorD[ultimo]-vectorD[0])>=dmin))
                {
                    //recorro el vector marcha atrás para utilizar en el ajuste
                    //al menos 3 elementos
                    for(j=ultimo-2;j>=0;j--)
                    {
                        //primer punto a utilizar
                        primero = j;
                        //compruebo si se supera la distancia máxima
                        if((vectorD[ultimo]-vectorD[primero])>=dmax)
                        {
                            //salimos del bucle
                            break;
                        }
                    }
                    //inicialmente los pesos los tomo como la unidad
                    for(j=0;j<datosMax;j++)
                    {
                        pesos[j] = 1.0;
                    }
                    //calculo la pendiente sin usar pesos (se actualizan a y b)
                    ajustaMMCC();
                    //comprobamos si hay que volver a ajustar
                    if(buscaEGros&&vuelveAAjustar())
                    {
                        //reajustamos con la nueva matriz de pesos
                        ajustaMMCC();
                    }
                    //distancia entre los extremos del segmento
                    d = vectorD[ultimo]-vectorD[primero];
                    //incremento de tiempos en horas
                    t = (vectorT[ultimo]-vectorT[primero])/3600.0;
                    //distingo entre cálculo con presiones y con altitudes
                    if((vt==0)||(vt==1))
                    {
                        //presiones en los puntos inicial y final del intervalo
                        //(deshaciendo el cambio de referirlas al punto inicial)
                        p1 = vectorHP[primero]+b;
                        p2 = vectorHP[primero]+
                             a*(vectorD[ultimo]-vectorD[primero])+b;
                        //incremento de altitudes por el incremento de presión
                        //https://www.math24.net/barometric-formula/
                        dh = -Mat.ln(p2/p1)/0.00011855d;
                        //pendiente
                        pend = dh/d*100.0;
                        //velocidad vertical
                        vver = dh/t;
                    }
                    else if(vt==2)
                    {
                        //la pendiente es el parámetro a de la recta
                        pend = a*100.0;
                        //velocidad vertical
                        vver = d*a/t;
                    }
                    //Compruebo si trabajamos en el sistema imperial
                    if(usaSI==0)
                    {
                        //pasamos la velocidda de ascenso vertical a pies/h
                        vver /= 0.3048;
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
    function onUpdate(dc)
    {
        // Set the background color
        View.findDrawableById("Background").setColor(getBackgroundColor());
        //variables para escribir los valores de trabajo
        var valuePend=View.findDrawableById("valuePend");
        var valueVver=View.findDrawableById("valueVver");
        var labelPend=View.findDrawableById("labelPend");
        var labelVverM=View.findDrawableById("labelVverM");
        var labelVverFT=View.findDrawableById("labelVverFT");
        var labelVverH=View.findDrawableById("labelVverH");
        //color del texto con respecto al fondo
        if(getBackgroundColor()==Gfx.COLOR_BLACK)
        {
            //blanco sobre fondo negro
            valuePend.setColor(Gfx.COLOR_WHITE);
            valueVver.setColor(Gfx.COLOR_WHITE);
            labelPend.setColor(Gfx.COLOR_WHITE);
            labelVverM.setColor(Gfx.COLOR_WHITE);
            labelVverFT.setColor(Gfx.COLOR_WHITE);
            labelVverH.setColor(Gfx.COLOR_WHITE);
        }
        else
        {
            //negro sobre fondo blanco
            valuePend.setColor(Gfx.COLOR_BLACK);
            valueVver.setColor(Gfx.COLOR_BLACK);
            labelPend.setColor(Gfx.COLOR_BLACK);
            labelVverM.setColor(Gfx.COLOR_BLACK);
            labelVverFT.setColor(Gfx.COLOR_BLACK);
            labelVverH.setColor(Gfx.COLOR_BLACK);
        }
        //escribo los datos
        valuePend.setText(pend.format("%.1f"));
        valueVver.setText(vver.format("%d"));
        //escribo las etiquetas
        labelPend.setText(Rez.Strings.labelPend);
        if(usaSI)
        {
            labelVverM.setText(Rez.Strings.labelVverM);
        }
        else
        {
            labelVverFT.setText(Rez.Strings.labelVverFT);
        }
        labelVverH.setText(Rez.Strings.labelVverH);
        // Call parent's onUpdate(dc) to redraw the layout
        View.onUpdate(dc);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Set your layout here. Anytime the size of obscurity of
    // the draw context is changed this will be called.
    function onLayout(dc)
    {
        //esto no sé lo que hace
        View.setLayout(Rez.Layouts.MainLayout(dc));
        //variables para configurar la visualización
        View.findDrawableById("valuePend");
        View.findDrawableById("valueVver");
        View.findDrawableById("labelPend");
        if(usaSI)
        {
            View.findDrawableById("labelVverM");
        }
        else
        {
            View.findDrawableById("labelVverFT");
        }
        View.findDrawableById("labelVverH");
        ////////////////////////////////////////////////////////////////////////
        return true;
    }
}
