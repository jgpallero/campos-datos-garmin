using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.UserProfile as Upf;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class zpulView extends Ui.DataField
{
    //zonas de pulsaciones
    var zp=null;
    //pulso en reposo
    var prep=0;
    //pulso medio
    var pmed=0;
    //datos a visualizar: pulso instantáneo y zonas
    var puls=0;
    var zins=0.0;
    var zmed=0.0;
    //variable auxiliar
    var aux=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función de inicialización del campo de datos
    function initialize()
    {
        //inicializo el campo
        DataField.initialize();
        //extraigo el pulso en reposo
        prep = Upf.getProfile().restingHeartRate;
        //comprobamos errores en la lectura
        if(prep==null)
        {
            //asignamos el valor 1 a la variable
            prep = 1;
        }
        //extraemos las zonas de pulsaciones
        zp = Upf.getHeartRateZones(Upf.getCurrentSport());
        //comprobamos errores en la lectura
        if(zp==null)
        {
            //creamos el vector y asignamos el valor 1
            zp = new[6];
            zp[0] = 1;
            zp[1] = 1;
            zp[2] = 1;
            zp[3] = 1;
            zp[4] = 1;
            zp[5] = 1;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función para calcular la zona de pulso donde estamos
    function calcZona(pulso)
    {
        //voy comprobando las zonas
        if((pulso>=prep)&&(pulso<=zp[1]))
        {
            //zona 1
            aux = (zp[1]-prep)*1.0;
            return 1.0+(pulso-prep)/aux;
        }
        else if((pulso>zp[1])&&(pulso<=zp[2]))
        {
            //zona 2
            aux = (zp[2]-zp[1])*1.0;
            return 2.0+(pulso-zp[1]-1)/aux;
        }
        else if((pulso>zp[2])&&(pulso<=zp[3]))
        {
            //zona 3
            aux = (zp[3]-zp[2])*1.0;
            return 3.0+(pulso-zp[2]-1)/aux;
        }
        else if((pulso>zp[3])&&(pulso<=zp[4]))
        {
            //zona 4
            aux = (zp[4]-zp[3])*1.0;
            return 4.0+(pulso-zp[3]-1)/aux;
        }
        else
        {
            //zona 5
            aux = (zp[5]-zp[4])*1.0;
            return 5.0+(pulso-zp[4]-1)/aux;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // The given info object contains all the current workout information.
    // Calculate a value and save it locally in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    function compute(info)
    {
        // See Activity.Info in the documentation for available information.
        //compruebo si tenemos pulso
        if(info has :currentHeartRate)
        {
            //extraigo el pulso instantáneo
            puls = info.currentHeartRate;
            //compruebo si el dato se ha leído
            if(puls!=null)
            {
                //zona del pulso instantáneo
                zins = calcZona(puls);
                ////////////////////////////////////////////////////////////////
                //compruebo si hay pulso medio
                if(info has :averageHeartRate)
                {
                    //extraigo el pulso medio
                    pmed = info.averageHeartRate;
                    //compruebo si el dato se ha leído
                    if(pmed!=null)
                    {
                        //zona del pulso medio
                        zmed = calcZona(pmed);
                    }
                }
            }
            else
            {
                //si no se ha leído devolvemos el valor 0
                puls = 0;
            }
        }
        else
        {
            //si no se ha leído devolvemos el valor 0
            puls = 0;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
    function onUpdate(dc)
    {
        // Set the background color
        View.findDrawableById("Background").setColor(getBackgroundColor());
        //variables para escribir los valores de trabajo
        var labelPulso=View.findDrawableById("labelPulso");
        var valuePulso=View.findDrawableById("valuePulso");
        var labelPulsoP1=View.findDrawableById("labelPulsoP1");
        var labelPulsoP2=View.findDrawableById("labelPulsoP2");
        var labelPulsoM=View.findDrawableById("labelPulsoM");
        var labelZonaI=View.findDrawableById("labelZonaI");
        var valueZonaI=View.findDrawableById("valueZonaI");
        var labelZonaM=View.findDrawableById("labelZonaM");
        var valueZonaM=View.findDrawableById("valueZonaM");
        //color del texto con respecto al fondo
        if(getBackgroundColor()==Gfx.COLOR_BLACK)
        {
            //blanco sobre fondo negro
            labelPulso.setColor(Gfx.COLOR_WHITE);
            valuePulso.setColor(Gfx.COLOR_WHITE);
            labelPulsoP1.setColor(Gfx.COLOR_WHITE);
            labelPulsoP2.setColor(Gfx.COLOR_WHITE);
            labelPulsoM.setColor(Gfx.COLOR_WHITE);
            labelZonaI.setColor(Gfx.COLOR_WHITE);
            valueZonaI.setColor(Gfx.COLOR_WHITE);
            labelZonaM.setColor(Gfx.COLOR_WHITE);
            valueZonaM.setColor(Gfx.COLOR_WHITE);
        }
        else
        {
            //negro sobre fondo blanco
            labelPulso.setColor(Gfx.COLOR_BLACK);
            valuePulso.setColor(Gfx.COLOR_BLACK);
            labelPulsoP1.setColor(Gfx.COLOR_BLACK);
            labelPulsoP2.setColor(Gfx.COLOR_BLACK);
            labelPulsoM.setColor(Gfx.COLOR_BLACK);
            labelZonaI.setColor(Gfx.COLOR_BLACK);
            valueZonaI.setColor(Gfx.COLOR_BLACK);
            labelZonaM.setColor(Gfx.COLOR_BLACK);
            valueZonaM.setColor(Gfx.COLOR_BLACK);
        }
        //escribo los datos
        valuePulso.setText(puls.format("%d"));
        valueZonaI.setText(zins.format("%.1f"));
        valueZonaM.setText(zmed.format("%.1f"));
        //escribo las etiquetas
        labelPulso.setText(Rez.Strings.labelPulso);
        labelPulsoP1.setText(Rez.Strings.labelPulsoP1);
        labelPulsoP2.setText(Rez.Strings.labelPulsoP2);
        labelPulsoM.setText(Rez.Strings.labelPulsoM);
        labelZonaI.setText(Rez.Strings.labelZonaI);
        labelZonaM.setText(Rez.Strings.labelZonaM);
        // Call parent's onUpdate(dc) to redraw the layout
        View.onUpdate(dc);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Set your layout here. Anytime the size of obscurity of
    // the draw context is changed this will be called.
    function onLayout(dc)
    {
        //esto no sé lo que hace
        View.setLayout(Rez.Layouts.MainLayout(dc));
        //variables para configurar la visualización
        View.findDrawableById("labelPulso");
        View.findDrawableById("valuePulso");
        View.findDrawableById("labelPulsoP1");
        View.findDrawableById("labelPulsoP2");
        View.findDrawableById("labelPulsoM");
        View.findDrawableById("labelZonaI");
        View.findDrawableById("valueZonaI");
        View.findDrawableById("labelZonaM");
        View.findDrawableById("valueZonaM");
        ////////////////////////////////////////////////////////////////////////
        return true;
    }
}
