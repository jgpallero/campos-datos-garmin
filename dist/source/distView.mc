using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class distView extends Ui.DataField
{
    //unidades del sistema internacional
    var usaSI=1;
    //distancia recorrida
    var dist=0.0;
    var distKm=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //función de inicialización del campo de datos
    function initialize()
    {
        //inicializo el campo
        DataField.initialize();
        //extraigo la configuración del dispositivo
        if(Sys.getDeviceSettings().distanceUnits==Sys.UNIT_STATUTE)
        {
            usaSI = 0;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // The given info object contains all the current workout information.
    // Calculate a value and save it locally in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    function compute(info)
    {
        //Compruebo si está la información que busco
        if(info has :elapsedDistance)
        {
            //Extraigo la distancia recorrida
            dist = info.elapsedDistance;
            //Compruebo posibles errores
            if(dist==null)
            {
                dist = 0.0;
            }
            //Tipo de unidades
            if(usaSI)
            {
                //Convierto la distancia en kilómetros
                distKm = dist/1000.0;
            }
            else
            {
                //convierto a unidades imperiales: pies y millas
                dist /= 0.3048;
                distKm = dist/5280.0;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
    function onUpdate(dc)
    {
        // Set the background color
        View.findDrawableById("Background").setColor(getBackgroundColor());
        //variables para escribir los valores de trabajo
        var valueDist=View.findDrawableById("valueDist");
        var labelDistM=View.findDrawableById("labelDistM");
        var labelDistKM=View.findDrawableById("labelDistKM");
        var labelDistFT=View.findDrawableById("labelDistFT");
        var labelDistMI=View.findDrawableById("labelDistMI");
        //color del texto con respecto al fondo
        if(getBackgroundColor()==Gfx.COLOR_BLACK)
        {
            //blanco sobre fondo negro
            valueDist.setColor(Gfx.COLOR_WHITE);
            labelDistM.setColor(Gfx.COLOR_WHITE);
            labelDistKM.setColor(Gfx.COLOR_WHITE);
            labelDistFT.setColor(Gfx.COLOR_WHITE);
            labelDistMI.setColor(Gfx.COLOR_WHITE);
        }
        else
        {
            //negro sobre fondo blanco
            valueDist.setColor(Gfx.COLOR_BLACK);
            labelDistM.setColor(Gfx.COLOR_BLACK);
            labelDistKM.setColor(Gfx.COLOR_BLACK);
            labelDistFT.setColor(Gfx.COLOR_BLACK);
            labelDistMI.setColor(Gfx.COLOR_BLACK);
        }
        //distingo entre distancias menores y mayores a 1 km/milla
        if(distKm<1.0)
        {
            //escribo el dato
            valueDist.setText(dist.format("%d"));
            //escribo las etiquetas
            if(usaSI)
            {
                labelDistM.setText(Rez.Strings.labelDistM);
            }
            else
            {
                labelDistFT.setText(Rez.Strings.labelDistFT);
            }
        }
        else
        {
            //escribo el dato
            valueDist.setText(distKm.format("%.2f"));
            //escribo las etiquetas
            if(usaSI)
            {
                labelDistKM.setText(Rez.Strings.labelDistKM);
            }
            else
            {
                labelDistMI.setText(Rez.Strings.labelDistMI);
            }
        }
        // Call parent's onUpdate(dc) to redraw the layout
        View.onUpdate(dc);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // Set your layout here. Anytime the size of obscurity of
    // the draw context is changed this will be called.
    function onLayout(dc)
    {
        //esto no sé lo que hace
        View.setLayout(Rez.Layouts.MainLayout(dc));
        //distingo entre distancias menores que 1 km/milla y < que 100 km/millas
        if(distKm<1.0)
        {
            //activo las etiquetas
            if(usaSI)
            {
                View.findDrawableById("labelDistM");
            }
            else
            {
                View.findDrawableById("labelDistFT");
            }
        }
        else
        {
            //activo las etiquetas
            if(usaSI)
            {
                View.findDrawableById("labelDistKM");
            }
            else
            {
                View.findDrawableById("labelDistMI");
            }
        }
        //activo el título y el valor
        View.findDrawableById("valueDist");
        ////////////////////////////////////////////////////////////////////////
        return true;
    }
}
